#!/usr/bin/perl
use utf8;
use 5.014; # Perl 5.14 introduced special case-folding classes

# 3 case-folding interpretations:
    # 1) ASCII - /a option
    # 2) Unicode - /u option
    # 3) locale - /l option
# use these to tell perl how to handle regexes regardless of what else is going on in the program

if (/\w+/a) { # A-Z, a-z, 0-9, _
    say 'Just ASCII here';
}
if (/\w+/u) { # any Unicode word character
    say 'Unicode too';
}
if (/\w+/l) { # The ASCII version, and word chars from the locale,
              # perhaps characters like Œ from Latin-9
    say 'I may just not print';
}

# case folding sucks without modifiers
/k/aai;     # only matches the ASCII K or k, not Kelvin sign
/k/aia;     # the /a's don't need to be next to each other
/ss/aai;    # only matches ASCII ss, SS, sS, Ss, not ß
/ff/aai;    # only matches ASCII ff, FF, fF, Ff, not ﬀ

# locales make things difficult
$_ = <STDIN>;

my $OE = chr( 0xBC ); # get exactly what we intend

if (/$OE/i) { # case-insensitive??? Maybe not.
    print "Found $OE\n";
}

# force Perl to use the locale's rules
$_ = <STDIN>;

my $OE = chr( 0xBC ); # get exactly what we intend

if (/$OE/li) {        # that's better
    print "Found $OE\n";
}

# okay, j/k; use Unicode instead
$_ = <STDIN>;
if (/Œ/ui) {   # now uses Unicode
    print "Found Œ\n";
}

# use \A for absolute beginning and \z for absolute end of string
$_ = 'my goodness fred';
if (/\Amy go/) {
    say 'found "my go" at the beginning';
}
if (/ss fred\z/) {
    say 'found "ss fred" at the end';
}

$_ = "here's a line you might enjoy
well, make that two lines
I think I'll add a third, just for kicks";
if (/enjoy$/m) {
    say "there's a line that ends 'enjoy', but that's not the end of string";
}
if (/kicks\z/) {
    say 'kicks at the end';
}
if (/\Ahere/) {
    say '"here" starts the string';
}
if (/^well,/m) {
    say 'some line starts well';
}
